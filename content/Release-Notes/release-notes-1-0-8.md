---
title: "Release Notes 1.0.8"
date: 2020-05-20T09:20:46+02:00
draft: false
---

### Bobobo

> NPM 1.0.8 version

- Added scale to the charts
- Added titles to the charts
- Created Terrain component
- Added time evolution commit by commit for codecity (from present to past)
