---
linktitle: "guide_ui_nav_bar"
date: 2020-08-28T15:23:15+02:00
title: How to use UI Navigation Bar
draft: false
categories: [ "Tutorials", "Documentation" ]
tags: ["api", "components", "guide", "demo"]
---

>[Remix this example on Glitch](). Or [view the demo]().

En este tutorial vamos a aprender a añadir el componente `ui_navigation_bar` en nuestros proyectos.

