---
linktitle: "guide_totem"
date: 2020-08-27T13:04:25+02:00
title: How to use Totem Component 
draft: false
categories: [ "Tutorials", "Documentation" ]
tags: ["api", "components", "guide", "demo"]
---

>[Remix this example on Glitch](https://glitch.com/~babia-totem-guide). Or [view the demo](https://babia-totem-guide.glitch.me).

Let's learn how to create a totem component and link it to different babia-components.

`geototemchart` component creates a totem, which can list several data files and allows us to change values represented in babia-components.

![image](../diagram_totem.jpg)

>Note: for more information about data file format check this [guide](../how_to_format_data_to_babiaxr/).

In this walkthrough, we are adding 2 babia-components, a totem with 3 data files and we will link them. 

Currently, we need all components having the same data format. In this example we are using `babia-piechart` and `babia-simplebarchart` because both of them use `key` and `size`.

We begin creating the scene:
```
<!DOCTYPE html>
<html>
  <head>
    <script src="https://aframe.io/releases/1.0.4/aframe.min.js"></script>
    <script src="https://unpkg.com/aframe-babia-components/dist/aframe-babia-components.min.js"></script>
  </head>

  <body>
    <a-scene>

    </a-scene>
  </body>
</html>
```

## One Chart with one Totem

Let's begin adding `babia-piechart` to the scene and manually inserting data.

```
<a-entity id="pie" geopiechart='legend: true; 
        data: [{"key":"kbn_network","size":10},{"key":"Maria","size":5},{"key":"Dave","size":9},{"key":"Jhon","size":12},{"key":"Sara","size":16},{"key":"Lemar","size":2},{"key":"Dawn","size":1},{"key":"Jesus","size":8},{"key":"Bitergia","size":3},{"key":"URJC","size":6},{"key":"Alice","size":22},{"key":"Pete","size":2},{"key":"Seth","size":6},{"key":"Martin","size":9}]'>
</a-entity>
```
It's important specifying `id` in order to link the component to the totem later.

>Note: this data will be missing when we select any file in the totem, so this method is not recommended.


Next step is adding the totem. Read the component [documentation](https://github.com/babiaxr/aframe-babia-components/blob/master/README.md#babia-totemchart-component).

### Attributes

- **charts_id**: chart identifier list and which component type are.

```
charts_id: [{"id": "pie", "type": "babia-piechart"}]
```

- **data_list**: list with the datafiles we want to add to the totem. These can be external data or imported with 'querier_json'.

In this example we will use 3 files: [data1.json](https://raw.githubusercontent.com/babiaxr/aframe-babia-components/master/examples/totems/totem_1_querier/data_examples/data1.json), [data2.json](https://raw.githubusercontent.com/babiaxr/aframe-babia-components/master/examples/totems/totem_1_querier/data_examples/data2.json) and [data3.json](https://raw.githubusercontent.com/babiaxr/aframe-babia-components/master/examples/totems/totem_1_querier/data_examples/data3.json).

For the moment those files haven't been imported, so we need to use `path` attribute.

```
data_list: [{"data":"Data 1", "path": "./data1.json"},
            {"data": "Data 2", "path": "./data2.json"}, 
            {"data": "Data 3", "path": "./data3.json"}]
```

Then we add the totem:

```
<a-entity id="totem" geototemchart='
        charts_id: [{"id": "pie", "type": "babia-piechart"}];
        data_list: [{"data":"Data 1", "path": "./data1.json"},
        {"data": "Data 2", "path": "./data2.json"}, 
        {"data": "Data 3", "path": "./data3.json"}]
position="-5 2 4"></a-entity>
```


If you select any option, the chart changes.

## Two Charts with one Totem

Now we are going to add another component, but this time we will import a data file with `querier_json`.

We start importing the file (don't forget the `id`):

```
<a-entity id="data1" querier_json="url: ./data1.json;"></a-entity>
```

Then we add `babia-simplebarchart` component using `filterdata` and `vismapper`:
```
<a-entity id="bars" geosimplebarchart='legend: true' filterdata="from: data1" vismapper="x-axis: key; height: size" 
    position="0 0 5"></a-entity>
```


>Note: for more information about querier_json, filterdata or vismapper here you have [the guide](../how_to_use_querier_filter_vismapper)

Last step, we link the component to the totem. As this is a vismapper component, we have to add the type:

```
charts_id: [{"id": "pie", "type": "babia-piechart"},
            {"id": "bars", "type": "vismapper"}]
```

We also have to change path attribute for `from_querier` because we have imported data1.json with `querier_json`.

```
data_list: [{"data":"Data 1", "from_querier": "data1"}, 
            {"data": "Data 2", "path": "./data2.json"}, 
            {"data": "Data 3", "path": "./data3.json"}]
```

Then we have:
```
<a-entity id="totem" geototemchart='
        charts_id: [{"id": "pie", "type": "babia-piechart"},
        {"id": "bars", "type": "vismapper"}];
        data_list: [{"data":"Data 1", "from_querier": "data1"},
        {"data": "Data 2", "path": "./data2.json"}, 
        {"data": "Data 3", "path": "./data3.json"}]
position="-5 2 4"></a-entity>
```