---
linktitle: "guide_querier_filter_mapper"
date: 2020-08-26T17:32:05+02:00
title: How to use Querier/Filter/Mapper 
draft: false
categories: [ "Tutorials", "Documentation" ]
tags: ["api", "components", "guide", "demo"]
---

>[Remix this example on Glitch](https://glitch.com/~example-querier). Or [view the demo](https://example-querier.glitch.me).

In this tutorial we are learning how to use `querier_json`, `filterdata`y `vismapper` components.

Let's define each component purpose:

- **querier_json**: it imports external JSON files.
- **filterdata**: it filters the data from the file.
- **vismapper**: it takes and maps the result from `filterdata` in order to represent them.

![image](https://gitlab.com/babiaxr/aframe-babia-components/uploads/f49795d4f0cddcca9aedc00f3afe2d18/Captura_de_pantalla_2020-07-27_a_las_12.57.49.png)

As we can see in the diagram, the `querier_json` component is inside an independent entity. Meanwhile `filterdata` and `vismapper` are in the same entity of babia-component.

For this tutorial we are adding a `babia-simplebarchart` to the scene.

First we create our scene:

```
<!DOCTYPE html>
<html>
  <head>
    <script src="https://aframe.io/releases/1.0.4/aframe.min.js"></script>
    <script src="https://unpkg.com/aframe-babia-components/dist/aframe-babia-components.min.js"></script>
  </head>

  <body>
    <a-scene>

    </a-scene>
  </body>
</html>
```

## Querier_JSON

As we mentioned before, the component `querier_json` will import JSON files with the data.

We need a JSON file with data, let's use [this](https://gitlab.com/babiaxr/aframe-babia-components/-/raw/master/examples/charts_querier/simplebar_chart_querier/data.json). You can use other files if you want.

>Note: If you want to know how to create data files for BabiaXR check [this guide](../how_to_format_data_to_babiaxr/).

For now, we want to know just the file url. Also we'll give an identificator to the component.

```
<a-entity id="data" querier_json="url: ./data.json;"></a-entity>
```

## Filterdata

Now that we have the JSON file imported, we are adding `babia-simplebarchart` and `filterdata` components.

`filterdata` prepares the data and filters in order to map them later. We need to add the `from` attribute with our identificator (same as `querier_json`) to obtain the file.

```
<a-entity babia-simplebarchart='legend: true; axis: true' filterdata="from: data" ></a-entity>
```

In this case (no filter specified), it will return all the data.

If we want to filter (ex. only data with name David) we need to use `filter` attribute.

```
<a-entity babia-simplebarchart='legend: true; axis: true' filterdata="from: data; filter: name=David" ></a-entity>
```
It returns elements with name = David.

## Vismapper

Last we'll add `vismapper` component, which will map our data to `babia-simplebarchart` component format.

This guide shows each component properties [link]

In this example, babia-simplebarchart expets the data types: `height` and `x_axis`; but in our JSON we have: `name', 'name2`, `size` and `height`.

- `height` numeric type.
- `x_axis` it represents tags (string type but it can be a number).

We can map them this way:

- height => size
- x_axis => name

Then we have as result:
```
<a-entity babia-simplebarchart='legend: true; axis: true' filterdata="from: data; filter: name=David" vismapper='x_axis: name; height: size' ></a-entity>
```