---
name: "Andrea Villaverde Hernández"
weight: 30
draft: false
avatar: "images/andrea.jpeg"
github : "camichan"
gitlab : "camichan"
linkedin: "andrea-villaverde-hernández-95794144"
twitter: "camichan"
mail: "camaratomoyo@outlook.com"
---
Bachelor's Degree student at University Rey Juan Carlos