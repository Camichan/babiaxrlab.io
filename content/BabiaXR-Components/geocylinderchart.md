---
#date: 2020-06-19T12:58:28+02:00
linktitle: Geocylinderchart
title: Geocylinderchart Component
draft: false
weight: 50
categories: [ "Components", "Documentation" ]
tags: ["api", "data format", "demo"]
---

This component must be used with one of the `vismapper` components, with the `x-axis`, `height` and `radius` attribute defined.

This component shows a cylinder chart.

[Demo](https://babiaxr.gitlab.io/aframe-babia-components/examples/charts/cylinder_chart/)

#### API

| Property        | Description           | Type   | Default value |
| --------        | -----------           | ----   | ----- |
| legend          | Shows a legend when hovering a cylinder  | boolean | false |
| axis          | Shows chart axis  | boolean | true |
| scale          | Scales up the chart. For example: scale 1/100 => `scale: 100` | number | - |
| heightMax          | Adjusts the height of the chart.  | number | - |
| radiusMax          | Adjusts bubbles' radius of the chart.  | number | - |
| palette          | Color palette of the chart `blues` `bussiness` `sunset`. [See more](#color-palettes)  | string | ubuntu |
| title          | Shows chart title  | string | - |
| titleFont          | Font of the title. Path to a typeface.json file or selector to `<a-asset-item>`. [See more](#fonts)  | JSON (list of objects) | [helvetiker_regular.typeface.json](https://rawgit.com/supermedium/superframe/master/components/text-geometry/lib/helvetiker_regular.typeface.json) |
| titleColor          | Color of the title  | string | #FFFFFF |
| titlePosition          | Position of the title  | string | 0 0 0 |
| animation          | Animates chart   | boolean | false |
| data          | Data to show with the chart  | JSON (list of objects) | - |

#### Data format
```json
[{"key":"David","height":9,"radius":1},
{"key":"David","height":8,"radius":3},
...
]

```

